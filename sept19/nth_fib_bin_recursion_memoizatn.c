	/*	program to speed up the standard binary recursive fibonacci algorithm  using memoization	*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

#define NFIB	n-1			/* size of the array  */

long fibonacci(int,long [],int);


main()
{
	int n;
	int i=0;
	int index=0;
	long fibarray[NFIB];		/* array to store fibonacci number */

	printf("\nentr n : ");
	scanf("%d",&n);

	while(i<NFIB)			/* setting each element to 0 */
		fibarray[i++]=0;

	printf("\n\n%dth fibonacci number is %ld\n\n",n,fibonacci(n,fibarray,index));
}

long fibonacci(int n,long fibarray[],int index)
{

	if(n<2)
		return n;
	else
		return (fibarray[index]  = fibonacci(n-1,fibarray,index+1)) + (fibarray[index+1] ? fibarray[index+1] : fibonacci(n-2,fibarray,index+1));
}
