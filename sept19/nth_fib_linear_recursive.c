/*	program to find the nth fibonacci number using linear recursion		*/

#include<stdio.h>

long fib(int n,int i,int j);

main()
{
	int n;

	printf("\nentr n : ");
	scanf("%d",&n);

	printf("\n\n%dth fibonacci number is %ld\n\n",n,fib(n,0,1));
}

long fib(int n,int i,int j)
{
	if(n==1)
		return i;
	else
		return fib(n-1,j,i+j);
}
