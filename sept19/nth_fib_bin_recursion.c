	/*	program to find nth fibonacci number using standard binary recursion		*/

#include<stdio.h>

long fib(int n);

main()
{
	int n;

	printf("\nentr n : ");
	scanf("%d",&n);

	printf("\n\n%dth fibonacci number is %ld\n\n",n,fib(n));
}

long fib(int n)
{
	if(n<2)
		return n;
	else
		return fib(n-1) + fib(n-2);
}
