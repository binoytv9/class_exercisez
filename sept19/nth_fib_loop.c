/*	program to find the nth fibonacci number using loop	*/

#include<stdio.h>

long fib(int n);

main()
{
	int n;

	printf("\nentr n : ");
	scanf("%d",&n);

	printf("\n\n%dth fibonacci number is %ld\n\n",n,fib(n));
}

long fib(int n)
{
	long i,j,k;

	i=0;
	j=1;

	while(1){
		k=i+j;
		if(n-- == 1)
			return i;
		i=j;
		j=k;
	}
}
