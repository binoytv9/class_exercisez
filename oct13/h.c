/*	Write a function which checks if its argument is a palindrome	*/

#include<stdio.h>
#include<string.h>

int is_palindrome(char *str);

main()
{
	char str[100],str1[100];

	printf("\nenter the string : ");
	scanf("%s",str);

	strcpy(str1,str);

	if(is_palindrome(str1)>0)
		printf("\n%s is palindrome !!!\n\n",str);
	else
		printf("\n%s is not palindrome !!!\n\n",str);
}

int is_palindrome(char *str)
{
	int len;

	if((len=strlen(str))<=1)
		return 1;
	
	if(*str != str[len-1])
		return 0;

	else{
		str[len-1]='\0';
		return is_palindrome(str+1);
	}
}
