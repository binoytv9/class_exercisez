/*	Write a function gcd(a, b) which computes the Greatest Common Divisor (or HCF, highest common factor) 
 *	of two numbers using Euclid's Algorithm
 */

#include<stdio.h>

int gcd(int x,int y);

main()
{
	int x,y;

	printf("\nenter x :");
	scanf("%d",&x);
	printf("\nenter y :");
	scanf("%d",&y);

	printf("\n\n gcd of %d and %d is %d\n\n",x,y,x>y ? gcd(x,y): gcd(y,x));
}

int gcd(int x,int y)
{
	if(x%y==0)
		return y;
	return gcd(x,x%y);
}
