/*	Write a function my_sine(x,n) which computes the sine of "x" using the sum of the 
 *	first 'n' terms of the infinite series expansion for sine. Note that "x" is a 
 *	floating point number and is in radians.
 */


#include<stdio.h>

double fact(int n);
double power(double x,int n);
double my_sine(double x,int n);

main()
{
	int n;
	double x,y;

	printf("\nenter x :");
	scanf("%lf",&x);
	printf("\nenter n :");
	scanf("%d",&n);

	y=x;
	x=(22/7)*(x/180);

	printf("\n\nsine of %f using the sum of the first %d terms of the infinite series expansion for sine is %f\n\n",y,n,my_sine(x,n));
}

double my_sine(double x,int n)
{
	if(n==0)
		return x;

	return power(-1,n)*power(x,2*n + 1)/fact(2*n + 1) + my_sine(x,n-1);
}

double power(double x,int n)
{
	if(n==0)
		return 1;

	return x*power(x,n-1);
}

double fact(int n)
{
	if(n==0)
		return 1;

	return n*fact(n-1);
}
