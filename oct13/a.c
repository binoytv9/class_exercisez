/*	Write a function:
 *
 *	void print(int a, int b) {
 *	...
 *	}
 *
 *	which will print all numbers from "a" to "b"
 */
#include<stdio.h>

void print(int a,int b);

main()
{
	int a,b;

	printf("\nenter a :");
	scanf("%d",&a);
	printf("\nenter b :");
	scanf("%d",&b);

	printf("\n\n");
	print(a,b);
	printf("\n\n");
}

void print(int a,int b)
{
	if(a>b)
		return;

	printf("\t%d",a);
	print(a+1,b);
}
