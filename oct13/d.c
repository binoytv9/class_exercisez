/*	Write a function raised_to(a, b) which computes "a to the power of b". Assume a and b are positive integers.
 */


#include<stdio.h>

unsigned raised_to(unsigned a,unsigned b);

main()
{
	unsigned a,b;

	printf("\nenter a :");
	scanf("%d",&a);
	printf("\nenter b :");
	scanf("%d",&b);

	printf("\n\n%u raise to the power of %u is %u\n\n",a,b,raised_to(a,b));
}

unsigned raised_to(unsigned a,unsigned b)
{
	if(b==0)
		return 1;

	return a*raised_to(a,b-1);
}
