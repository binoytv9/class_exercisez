/*	Write a function sum_series(a, b) which computes the sum of the arithmetic progression: a, a+1, a+2, ....., b.
 */

#include<stdio.h>

long sum_series(int a, int b);

main()
{
	int a,b;

	printf("\nenter a :");
	scanf("%d",&a);
	printf("\nenter b :");
	scanf("%d",&b);

	printf("\n\nthe sum of the arithmetic progression: %d, %d+1, %d+2, ....., %d is %ld\n\n",a,a,a,b,sum_series(a,b));
}

long sum_series(int a, int b)
{
	if(a == b)
		return a;

	return a + sum_series(a+1,b);
}
		
