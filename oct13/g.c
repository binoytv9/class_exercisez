/*	Write a function reverse_str(char *a) which reverses the alphabets stored in the string pointed to by "a".	*/


#include<stdio.h>
#include<string.h>

void reverse_str(char *a,int len);

main()
{
	int len;
	char a[100];

	printf("\nenter the string :");
	scanf("%s",a);

	len=strlen(a);

	reverse_str(a,len);

	printf("\n\nreversed string is '%s'\n\n",a);
}

void reverse_str(char *a,int len)
{
	char tmp;

	if(len<=1)
		return;

	tmp=*a;
	*a=*(a+len-1);
	*(a+len-1)=tmp;

	reverse_str(a+1,len-2);
}
