/*	Write a function sum_digits(n) which computes sum of the digits of "n"	*/

#include<stdio.h>

int sum_digits(int n);

main()
{
	int n;

	printf("\nenter n : ");
	scanf("%d",&n);

	printf("\n\nsum of the digits of %d is %d\n\n",n,sum_digits(n));
}

int sum_digits(int n)
{
	if(n<10)
		return n;

	return n%10 + sum_digits(n/10);
}
