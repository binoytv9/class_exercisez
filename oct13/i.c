/*	Write a simple "factorial" function	*/


#include<stdio.h>

long fact(int n);

main()
{
	int n;

	printf("\nenter n :");
	scanf("%d",&n);

	printf("\n\nfactorial of %d is %ld\n\n",n,fact(n));
}

long fact(int n)
{
	if(n==0)
		return 1;

	return n * fact(n-1);
} 
